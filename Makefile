CC=gcc
CFLAGS=-I.

SRC_DIR = ./src
BUILD_DIR = ./build
FIG_DIR=./images
TXT_DIR=./text

#
# Utility functions
#
crypto: ${SRC_DIR}/crypto.c ${SRC_DIR}/crypto.h ${BUILD_DIR}
	$(CC) -c ${SRC_DIR}/crypto.c -o ${BUILD_DIR}/crypto.o

image: ${SRC_DIR}/image.c ${SRC_DIR}/image.h ${BUILD_DIR}
	$(CC) -c ${SRC_DIR}/image.c -o ${BUILD_DIR}/image.o

utility: ${SRC_DIR}/utility.c ${SRC_DIR}/utility.h ${BUILD_DIR}
	$(CC) -c ${SRC_DIR}/utility.c -o ${BUILD_DIR}/utility.o

#
# Main file for each application
#
cryptography: ${SRC_DIR}/cryptography.c ${SRC_DIR}/crypto.h ${BUILD_DIR}
	$(CC) -c ${SRC_DIR}/cryptography.c -o ${BUILD_DIR}/cryptography.o

convert_to_grey: ${SRC_DIR}/convert_to_grey.c ${SRC_DIR}/image.h ${SRC_DIR}/utility.h ${BUILD_DIR}
	$(CC) -c ${SRC_DIR}/convert_to_grey.c -o ${BUILD_DIR}/convert_to_grey.o

#
# Link .o
#
image_crypto: crypto image utility cryptography ${BUILD_DIR} ${FIG_DIR}
	$(CC) ${BUILD_DIR}/utility.o ${BUILD_DIR}/crypto.o ${BUILD_DIR}/image.o ${BUILD_DIR}/cryptography.o $(CFLAGS) -o $(BUILD_DIR)/image_crypto

image_to_grey: convert_to_grey image utility ${BUILD_DIR} ${FIG_DIR}
	$(CC) ${BUILD_DIR}/convert_to_grey.o ${BUILD_DIR}/utility.o ${BUILD_DIR}/image.o -o $(BUILD_DIR)/image_grey

#
# mkdir if the directory does not exist
#
${BUILD_DIR}:
	@mkdir -p ${BUILD_DIR}

${FIG_DIR}:
	@mkdir -p ${FIG_DIR}

${TXT_DIR}:
	@mkdir -p ${TXT_DIR}
#
# Clean all compiled files
#
.PHONY: clean
clean:
	rm -f $(BUILD_DIR)/*