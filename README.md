# First Step into Cryptography
#### Author: [陳立馨 Li-Hsin Chen](https://www.linkedin.com/in/lhchen7180/) 
#### Video Demo:  [CS50 2023 Final Project by Li-Hsin Chen](https://www.youtube.com/watch?v=yr9AafVBDF4)
#### Description: 
This is a small application that encrypts a message into an image. It also has the capability to decrypt the message from an image if one also has the image "key".

To encrypt/decrypt the message, make sure the original image and the one 
that contains the message are stored under the folder `images`. 
Currently only images in the format of png are allowed.  

If your message is long, you can save it in a txt file under the folder `text`. 
Note that currently, the maximal length of a message is limited to 999 characters. Any character above the limit will not be encrypted.  

# Usage
## Encrypt/Decrypt
Firstly, open the terminal and copy this folder to your laptop/computer with  
`git clone https://gitlab.com/cs50_2023_icla/final_project.git`  
or simply download source code. 

Enter the project folder  
`cd final_project`  

Compile the application with the following command in your terminal.
`make image_crypto`  

To encrypt your message, run  
`./build/image_crypto -e <NAME_OF_ORIGINAL_IMAGE>.png -m <YOUR_MESSAGE>`  
or  
`./build/image_crypto -e <NAME_OF_ORIGINAL_IMAGE>.png -f <MESSAGE_FILE>`  
To decrypt the message, run  
`./build/image_crypto -d <NAME_OF_ORIGINAL_IMAGE>.png -p <NAME_OF_ENCODED_IMAGE>.png`  

The `-e` and `-d` flags tell the application whether to encrypt or decrypt 
the message. You can type the message in the command line with the `-m` flag or 
tell the application to read from a file with the `-f` flag. For decryption, 
add the `-p` flag and the name of the image that contains the message. 
Note that you do not have to type the full path of the image or the text file. 
Make sure to put the image in images folder and text file in text folder.

<!-- ## Convert to grey scale
You can also convert an image from color to grey scale.
Firstly, compile the application with the following command:  
`make image_to_grey`  

Then execute it with  
`./build/image_to_grey <NAME_OF_FILE_TO_CONVERT>` 

The converted image will be saved to images/<NAME_OF_FILE_TO_CONVERT>_grey.png
-->

# Acknowledgements
Throughout the course of this project, the help from open-resource intelligence is tremendous. Special thanks to all the contributors of [stb](https://github.com/nothings/stb) and those who are generous to write informative tutorials and solutions, such as [this](https://solarianprogrammer.com/2019/06/10/c-programming-reading-writing-images-stb_image-libraries/) and [this](https://stackoverflow.com/questions/48235421/get-rgb-of-a-pixel-in-stb-image
).

# LICENSE
This project is distributed under the MIT LICENSE. See LICENSE for more information.