#include <getopt.h>
#include <stdio.h>

#include "image.h"
#include "utility.h"

int main(int argc, char *argv[]) {
    // Ensure proper usage.
    if (argc != optind + 1) {
        printf("Usage: ./grey ORIGINAL_FIGURE\n");
        exit(1);
    }

    char *originalFile = argv[optind];
    char imgPath[256];
    sprintf(imgPath, "%s%s", IMAGE_DIR, originalFile);
    Image originalImg;
    loadImage(&originalImg, imgPath);

    /* Print RGB */
    // printRGB(&originalImg);

    /* Convert to grey scale and save as a new figure. */
    Image greyImg;
    convertToGreyScale(&originalImg, &greyImg);

    /* Save to a new figure. */
    char greyPath[256];
    char *end = strrchr(originalFile, '.');
    int nameLen = strlen(originalFile) - strlen(end);
    char greyName[nameLen + 1];
    int i;
    for (i = 0; i < nameLen; i++) {
        greyName[i] = originalFile[i];
    }
    greyName[nameLen] = '\0';

    sprintf(greyPath, "%s%s%s", IMAGE_DIR, greyName, "_grey.png");
    saveImage(&greyImg, greyPath);

    /* Free memory. */
    freeImage(&originalImg);
    freeImage(&greyImg);
}
