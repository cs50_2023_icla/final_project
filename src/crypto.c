#include "crypto.h"

#include <math.h>

void encryptMessage(Image *original, Image *encrypted, const char *message) {
    /* Set random seed. */
    srand(original->width);

    /* Declare constant variables. */
    const int maxLength = original->width * original->height;
    const unsigned bytePerPixel = original->channels;

    /* Variables for the charcters in the message. */
    int count = 0;
    int pos, rgb;
    unsigned char *thisOriginal, *thisEncrypted;
    /* Iterate over the message. */
    for (char c = *message; c != '\0'; c = *++message) {
        count += 1;

        /* To ASCII. Note that we limit characters from 32 - 126. */
        int n = c;
        if (n < 32 || n > 126) {
            printf(
                "WARNING: only characters correspond to ASCII number between "
                "32 and 126 are allowed. The characters outside this range will"
                "not be encrypted and will be lost.");
            n = 0;
        }

        /* Get location and which channel to store information of this
         * character. */
        pos = rand() % (maxLength - 3);
        rgb = rand() % 3;

        /* Compute the new value in the channel of this pixel. */
        thisOriginal = original->data + pos * bytePerPixel;
        thisEncrypted = encrypted->data + pos * bytePerPixel;
        thisEncrypted[rgb] = (thisOriginal[rgb] + n) % 256;

        if (count > 999) {
            printf(
                "WARNING: a maximum of 999 characters is allowed. The rest "
                "will be truncated.");
            break;
        }
    }

    if (count > 999) count = 999;
    int res = count, digit = 0;
    for (int i = 0; i < 3; i++) {
        /* Store the length of the message in the last 3 pixels. One digit in
         * one pixel. */

        digit = res % 10;
        res = res / 10;

        thisOriginal = original->data + (maxLength - 1 - i) * bytePerPixel;
        thisEncrypted = encrypted->data + (maxLength - 1 - i) * bytePerPixel;
        thisEncrypted[0] = (thisOriginal[0] + digit) % 256;
    }
}

void decryptMessage(Image *original, Image *encrypted) {
    /* Set random seed. */
    srand(original->width);

    /* Declare constant variables. */
    const int maxLength = original->width * original->height;
    const unsigned bytePerPixel = original->channels;

    /* Variables for the charcters in the message. */
    int len = 0;
    int n, pos, rgb;
    char c;
    unsigned char *thisOriginal, *thisEncrypted;

    /* Get length of the message. */
    for (int i = 0; i < 3; i++) {
        thisOriginal = original->data + (maxLength - 1 - i) * bytePerPixel;
        thisEncrypted = encrypted->data + (maxLength - 1 - i) * bytePerPixel;
        if (thisEncrypted[0] < thisOriginal[0]) {
            n = thisEncrypted[0] + 256 - thisOriginal[0];
        } else {
            n = thisEncrypted[0] - thisOriginal[0];
        }
        len += n * pow(10, i);
    }

    char message[len + 1];
    /* Iterate over the message. */
    for (int i = 0; i < len; i++) {
        /* Get location and which channel to store information of this
         * character. */
        pos = rand() % (maxLength - 3);
        rgb = rand() % 3;

        /* Compute the ASCII value of the character. */
        thisOriginal = original->data + pos * bytePerPixel;
        thisEncrypted = encrypted->data + pos * bytePerPixel;
        if (thisEncrypted[rgb] < thisOriginal[rgb]) {
            n = thisEncrypted[rgb] + 256 - thisOriginal[rgb];
        } else {
            n = thisEncrypted[rgb] - thisOriginal[rgb];
        }

        /* To ASCII. Note that we limit characters from 32 - 126. */
        if (n < 0 || n > 126) {
            printf(
                "WARNING: Something wrong with decryption. This character will "
                "not be decrypted.");
            c = ' ';
        } else if (n == 0) {
            /* The character was not properly encrypted. */
            c = ' ';
        } else {
            c = n + '\0';
        }
        message[i] = c;
    }
    message[len] = '\0';
    printf("Decrypted message: %s\n", message);
}
