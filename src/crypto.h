#include "image.h"
#include "utility.h"

/* Encrypt/Decrypt */
int getLocation(const int width, const int height);
void getMessageFromFile(const char* fname, char* message);
void encryptMessage(Image* original, Image* encrypted, const char* message);
void decryptMessage(Image* original, Image* encrypted);
