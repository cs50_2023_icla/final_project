#include <getopt.h>

#include "crypto.h"

int main(int argc, char *argv[]) {
    // Ensure proper usage.
    if (argc != optind + 4) {
        printf(
            "To encode: ./image_encrypt -e ORIGINAL_FIGURE -m MESSAGE\n"
            "OR         ./image_encrypt -e ORIGINAL_FIGURE -f MESSAGE_FILE\n");
        printf(
            "To decode: ./image_encrypt -d ORIGINAL_FIGURE -p "
            "ENCODED_FIGURE\n");
        exit(1);
    }

    /* Get what action the user wishes to use. */
    char *action = argv[optind];

    /* Get the name of the original file and check format. */
    char *originalFile = argv[optind + 1];
    if (checkPNG(originalFile) == false) {
        printf("ERROR: Currently only PNG files are supported.");
        exit(1);
    }
    char originalPath[256];
    sprintf(originalPath, "%s%s", IMAGE_DIR, originalFile);
    Image original;
    loadImage(&original, originalPath);

    /* Encryption. */
    if (strcmp(action, "-e") == 0) {
        /* Message in the command line argument. */
        char *type = argv[optind + 2];
        char *message;
        if (strcmp(type, "-m") == 0) {
            message = argv[optind + 3];
        }
        /* Message in a separate file. */
        else if (strcmp(type, "-f") == 0) {
            /* Get file name. */
            char *file = argv[optind + 3];

            /* Add text directory to file path. */
            char filePath[256];
            sprintf(filePath, "%s%s", TXT_DIR, file);
            printf("Message to encrypt is in %s\n", filePath);

            /* Read message from the file. */
            message = malloc(MAX_TXT_LEN * sizeof(char));
            getMessageFromFile(filePath, message);
        }
        /* Error handling. */
        else {
            printf("ERROR: unsupported flag type %s\n", type);
            exit(1);
        }
        printf("Message to encrypt: %s\n", message);

        /* Allocate memory for the encrypted image. */
        Image encrypted;
        allocateImageMemory(&encrypted, original.width, original.height,
                            original.channels);

        /* Copy data from the original.*/
        copyData(&original, &encrypted);

        /* Encryption */
        encryptMessage(&original, &encrypted, message);

        /* Get image name without file extension. */
        char *end = strrchr(originalFile, '.');
        int nameLen = strlen(originalFile) - strlen(end);
        char encryptedName[nameLen + 1];
        int i;
        for (i = 0; i < nameLen; i++) {
            encryptedName[i] = originalFile[i];
        }
        encryptedName[nameLen] = '\0';

        /* Save the encrypted image. */
        char encryptedPath[256];
        sprintf(encryptedPath, "%s%s%s", IMAGE_DIR, encryptedName,
                "_encrypted.png");
        saveImage(&encrypted, encryptedPath);

        /* Free memory. */
        if (strcmp(type, "-f") == 0) {
            free(message);
        }
        freeImage(&original);
        freeImage(&encrypted);
    }
    /* Decryption. */
    else if (strcmp(action, "-d") == 0) {
        char *encryptedFile = argv[optind + 3];
        printf("Message to decode is stored in: %s\n", encryptedFile);

        /* Only PNG files are accepted. */
        if (checkPNG(encryptedFile) == false) {
            printf("ERROR: Currently only PNG files are supported.");
            exit(1);
        }

        /* Load original and encrypted image. */
        Image original, encrypted;
        loadImage(&original, originalPath);
        char encryptedPath[256];
        sprintf(encryptedPath, "%s%s", IMAGE_DIR, encryptedFile);
        loadImage(&encrypted, encryptedPath);

        /* Decryption. */
        decryptMessage(&original, &encrypted);

        /* Free memory. */
        freeImage(&original);
        freeImage(&encrypted);
    }
    /* Error handling. */
    else {
        printf("ERROR: Unsupported action %s\n", action);
        exit(1);
    }
}