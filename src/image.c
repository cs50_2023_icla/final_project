#include "image.h"

#define STB_IMAGE_IMPLEMENTATION
#include "../stb_image/stb_image.h"

#define STB_IMAGE_WRITE_IMPLEMENTATION
#include "../stb_image/stb_image_write.h"

void loadImage(Image *img, const char *imgPath) {
    img->data = stbi_load(imgPath, &img->width, &img->height, &img->channels,
                          STBI_rgb_alpha);

    if (img->data == NULL) {
        printf("ERROR: error in loading image.");
        exit(1);
    }

    img->size = img->width * img->height * img->channels;
    img->allocationType = STB_ALLOCATED;
}

void allocateImageMemory(Image *img, int width, int height, int channels) {
    /* Allocate momory for data. */
    size_t size = width * height * channels;
    img->data = malloc(size);

    /* Information of the image. */
    if (img->data != NULL) {
        img->size = size;
        img->width = width;
        img->height = height;
        img->channels = channels;
        img->allocationType = SELF_ALLOCATED;
    }
}

void freeImage(Image *img) {
    if (img->data != NULL) {
        if (img->allocationType == STB_ALLOCATED) {
            stbi_image_free(img->data);

        } else {
            free(img->data);
        }
        img->data = NULL;
        img->width = 0;
        img->height = 0;
        img->size = 0;
    }
}

void printRGB(Image *img) {
    /* */
    int i, j;
    unsigned bytePerPixel = img->channels;
    unsigned char *pixelOffset;
    /* Iterate over pixels. */
    for (j = 0; j < img->height; j++) {
        for (i = 0; i < img->width; i++) {
            pixelOffset = img->data + (i + img->width * j) * bytePerPixel;
            unsigned char r = pixelOffset[0];
            unsigned char g = pixelOffset[1];
            unsigned char b = pixelOffset[2];
            unsigned char a = img->channels >= 4 ? pixelOffset[3] : 0xff;
            printf("pixel[%i][%i]: rgb(%i, %i, %i), a:%i\n", i, j, r, g, b, a);

            if ((i + img->width * j) > 100) {
                return;
            }
        }
    }
}

void convertToGreyScale(const Image *img, Image *greyImg) {
    if (!(img->channels >= 3)) {
        printf("ERROR: The input image must have at least 3 channels.");
        exit(1);
    }
    /* Declare constants. */
    int greyChannels = img->channels == 4 ? 2 : 1;
    allocateImageMemory(greyImg, img->width, img->height, greyChannels);
    if (greyImg->data == NULL) {
        printf("ERROR: Error allocating memory for new image.");
        exit(1);
    }

    /* Iterate over pixels and convert them to grey scale. */
    unsigned bytePerPixel = img->channels;
    unsigned bytePerGreyPixel = greyImg->channels;
    unsigned char *thisPixel, *thisGreyPixel;
    for (int i = 0; i < img->width; i++) {
        for (int j = 0; j < img->height; j++) {
            thisPixel = img->data + (i + img->width * j) * bytePerPixel;
            thisGreyPixel =
                greyImg->data + (i + greyImg->width * j) * bytePerGreyPixel;
            thisGreyPixel[0] =
                (thisPixel[0] + thisPixel[1] + thisGreyPixel[2]) / 3.;
            if (img->channels == 4) {
                thisGreyPixel[1] = thisPixel[3];
            }
        }
    }
}

void saveImage(const Image *img, const char *Path) {
    /* Save as a new figure.*/
    if (checkFileFormat(Path, ".png") || checkFileFormat(Path, ".PNG")) {
        stbi_write_png(Path, img->width, img->height, img->channels, img->data,
                       img->width * img->channels);
    } else {
        printf("ERROR: Unsupported format. Currently only PNG is supported.");
    }
}

void copyData(const Image *copyFrom, Image *copyTo) {
    /* Allocate memory. */
    allocateImageMemory(copyTo, copyFrom->width, copyFrom->height,
                        copyFrom->channels);

    int i, j;
    unsigned bytePerPixel = copyFrom->channels;
    unsigned char *pixelFrom, *pixelTo;
    /* Iterate over pixels. */
    for (j = 0; j < copyFrom->height; j++) {
        for (i = 0; i < copyFrom->width; i++) {
            pixelFrom =
                copyFrom->data + (i + copyFrom->width * j) * bytePerPixel;
            pixelTo = copyTo->data + (i + copyTo->width * j) * bytePerPixel;
            pixelTo[0] = pixelFrom[0];
            pixelTo[1] = pixelFrom[1];
            pixelTo[2] = pixelFrom[2];
            if (copyFrom->channels >= 4) {
                pixelTo[3] = pixelFrom[3];
            }
        }
    }
}
