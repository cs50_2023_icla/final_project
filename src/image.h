#include "utility.h"

enum AllocationType { NO_ALLOCATION, SELF_ALLOCATED, STB_ALLOCATED };

typedef struct {
    int width;
    int height;
    int channels;
    size_t size;
    uint8_t* data;
    enum AllocationType allocationType;
} Image;

/* Image related */
void loadImage(Image* img, const char* imgPath);
void allocateImageMemory(Image* img, int width, int height, int channels);
void freeImage(Image* img);
void saveImage(const Image* img, const char* Path);
void printRGB(Image* img);
void convertToGreyScale(const Image* img, Image* greyImg);
void copyData(const Image* copyFrom, Image* copyTo);
