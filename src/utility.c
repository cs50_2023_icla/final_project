#include "utility.h"

bool checkPNG(const char *fname) {
    if (checkFileFormat(fname, ".png") || checkFileFormat(fname, ".PNG")) {
        return true;
    } else {
        return false;
    }
}

bool checkJPG(const char *fname) {
    if (checkFileFormat(fname, ".jpg") || checkFileFormat(fname, ".JPG") ||
        checkFileFormat(fname, ".jpeg") || checkFileFormat(fname, ".JPEG")) {
        return true;
    } else {
        return false;
    }
}

bool checkFileFormat(const char *fname, const char *fmt) {
    /* Get the last occurance of '.' */
    char *nameEnd = strrchr(fname, '.');
    if (!nameEnd) {
        printf("ERROR: File does not have an extension to indicate format.\n");
        exit(1);
    }
    return !strcmp(nameEnd, fmt);
}

int getLocation(const int width, const int height) {
    /*
    The last pixel is used to store length of the message.
    Do not use it.

    The seed of random number generator is the width of the figure.
    */
    srand(width);
    int maxLength = width * height - 1;
    int pos = rand() % (maxLength + 1);

    return pos;
}

void getMessageFromFile(const char *fname, char *message) {
    FILE *file = fopen(fname, "r");
    if (file == NULL) {
        printf("ERROR: error loading file %s\n", fname);
        exit(1);
    }

    char line[MAX_TXT_LEN];
    fgets(line, MAX_TXT_LEN, file);

    strncpy(message, line, MAX_TXT_LEN);
    message[MAX_TXT_LEN - 1] = '\0';
}
