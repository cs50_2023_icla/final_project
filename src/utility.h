#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define IMAGE_DIR "./images/"
#define TXT_DIR "./text/"
#define MAX_TXT_LEN 999

/* General utiilty */
bool checkFileFormat(const char* fname, const char* fmt);
bool checkJPG(const char* fname);
bool checkPNG(const char* fname);
